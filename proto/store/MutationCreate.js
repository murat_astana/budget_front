function mutationCreate(state, payload){
  if (state.data == null)
    state.data = [];

  if (payload.data)
    state.data.splice(0, 0, payload.data);

  state.error = null;
  state.isLoading = false;
}

export {mutationCreate};
