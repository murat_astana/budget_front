
export let def_state = {
  data: null,
  error: null,
  isLoading: false,
  last_page: 1,
  current_page: 0,
};

export const def_mutations = {
  begin(state){
    state.error = null;
    state.isLoading = false;
  },
  success(state, payload){
    if (payload.data && state.data == null) {
      state.data = payload.data;
    }
    else if (payload.data) {
      state.data.push(...payload.data)
    }

    state.error = null;
    state.isLoading = false;
    state.last_page = payload.last_page;
    state.current_page = payload.current_page;
  },
  error(state, error){
    state.error = error;
    state.isLoading = false;
  },
  clear(state){
    state.data = null;
    state.last_page = 1;
    state.current_page = 0;
    state.error = null;
    state.isLoading = false;
  }
}

export const def_getters = {
  data: state => state.data,
  error: state => state.error,
  isLoading: state => state.isLoading,
  last_page: state => state.last_page,
  current_page: state => state.current_page
}


