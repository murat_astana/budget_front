function mutationUpdate(state, payload){
  if (payload.data) {
    let data = Object.create(state.data);
    let key = data.map(e => e.id).indexOf(payload.data.id);
    data[key] = payload.data;

    state.data = data;
  }

  state.error = null;
  state.isLoading = false;
}


export {mutationUpdate};
