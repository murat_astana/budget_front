function mutationDelete (state, id){
  if (id ){
    let key =  state.data.map(e => e.id).indexOf(id);
    state.data.splice(key, 1);
  }

  state.error = null;
  state.isLoading = false;
}

export {mutationDelete};
