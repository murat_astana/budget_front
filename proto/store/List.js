
export let def_state = {
  data: null,
  error: null,
  isLoading: false
};

export const def_mutations = {
  begin(state){
    state.error = null;
    state.isLoading = false;
  },
  success(state, payload){
    if (payload && 'data' in payload) {
      state.data = payload.data;
    }
    else if (payload) {
      state.data = payload;
    }
    state.error = null;
    state.isLoading = false;
  },
  error(state, error){
    state.error = error;
    state.isLoading = false;
  },
  clear(state){
    state.data = null;
    state.error = null;
    state.isLoading = false;
  }
}

export const def_getters = {
  data: state => state.data,
  isLoading: state => state.isLoading,
  error: state => state.error
}


