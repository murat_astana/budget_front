
import config from '../nuxt.config'

export default function ({ store, redirect, route, $router, $axios, $access}) {
  let need_check_auth = config.auth_middleware.except_route_name != route.name;
  if (!need_check_auth)
    return;

  let has_auth = $axios.defaults.headers.common.hasOwnProperty('Authorization');
  let token = store.getters["auth/token"];

  if (!has_auth && token) {
    $axios.setToken(token, 'Bearer')
    has_auth = true;
  }

  if (need_check_auth && !has_auth)
    redirect({name: config.auth_middleware.auth_route_name})

  if (!$access(route))
    redirect('/');
}
