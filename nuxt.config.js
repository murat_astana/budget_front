import colors from 'vuetify/es5/util/colors'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - budget',
    title: 'budget',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/notifications',
      ssr: false
    },
    {
      src: '~/plugins/vuex-persist',
      ssr: false
    },
    {
      src: '~/plugins/rules',
      ssr: false
    },
    {
      src: '~/plugins/axios',
      ssr: false
    },
    {
      src: '~/plugins/infiniteloading',
      ssr: false
    },
    {
      src: '~/plugins/dateparser',
      ssr: false
    },
    {
      src: '~/plugins/vuetify',
      ssr: false
    },
    {
      src: '~/plugins/number_format',
      ssr: false
    },
    {
      src: '~/plugins/valute_format',
      ssr: false
    },
    {
      src: '~/plugins/scrollbar',
      ssr: false
    },
    {
      src: '~/plugins/access',
      ssr: false
    },
  ],

  // router param
  router: {
    middleware: ['auth']
  },

  auth_middleware:{
    except_route_name: 'login',
    auth_route_name: 'login'
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },


  axios: {
    // extra config e.g
    // BaseURL: 'https://link-to-API'
    baseURL: process.env.API_URL,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
}
