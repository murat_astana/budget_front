#!/bin/sh
set -e

echo "Deploying application ..."

# Enter maintenance mode
# Update codebase
git fetch origin master
git reset --hard origin/master

# Install dependencies based on lock file
npm update --no-interaction --prefer-dist --optimize-autoloader

# Migrate database
# php artisan migrate --force

# Note: If you're using queue workers, this is the place to restart them.
# ...

# Clear cache
npm install
npm run generate


echo "Application deployed!"
