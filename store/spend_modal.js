import {def_state, def_mutations, def_getters} from '@/proto/store/ListWithPage'
import {mutationUpdate} from '@/proto/store/MutationUpdate'
import {mutationCreate} from '@/proto/store/MutationCreate'
import {mutationDelete} from '@/proto/store/MutationDelete'


const state =  () => ({...def_state});
const mutations = {...def_mutations}
const getters = {...def_getters}

mutations.update = mutationUpdate;
mutations.create = mutationCreate;
mutations.delete = mutationDelete;

const actions = {
  async list({ commit }, data){
    commit('begin');

    await this.$axios.get('/spend', {params:data})
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  }
}

getters.total_sum = function (state) {
  let main_total_sum = 0;
  for (let i in state.data){
    main_total_sum += state.data[i].total_sum;
  }

  return main_total_sum;
};

export {state, mutations, getters, actions};
