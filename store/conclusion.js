import {def_state, def_mutations, def_getters} from '@/proto/store/List'
import {mutationUpdate} from '@/proto/store/MutationUpdate'
import {mutationCreate} from '@/proto/store/MutationCreate'
import {mutationDelete} from '@/proto/store/MutationDelete'


const state =  () => ({...def_state});
const mutations = {...def_mutations}
const getters = {...def_getters}

mutations.update = mutationUpdate;
mutations.create = mutationCreate;
mutations.delete = mutationDelete;

const actions = {
  async list({ commit }, data){
    commit('begin');

    await this.$axios.get('/conclusion')
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  },
  async create({ commit }, data){
    commit('begin');

    await this.$axios.post('/conclusion', data)
      .then(response => commit('create', response.data))
      .catch(error => commit('error', error.data));
  },
  async update({ commit, store }, data){
    commit('begin');

    await this.$axios.post('/conclusion/' + data.id, data)
      .then(response => commit('update', response.data))
      .catch(error => commit('error', error.data));
  },
  async delete({ commit }, id){
    commit('begin');

    await this.$axios.delete('/conclusion/' + id)
      .then(response => commit('delete', id))
      .catch(error => commit('error', error.data));
  },
  async load({state, dispatch}){
    console.log(state);
    if (state.data != null)
      return true;

    await dispatch('conclusion');
  }
}

export {state, mutations, getters, actions};
