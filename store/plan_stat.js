import {def_state, def_mutations, def_getters} from '@/proto/store/List'


const state =  () => ({...def_state});
const mutations = {...def_mutations}
const getters = {...def_getters}

const actions = {
  async list({ commit }, data){
    commit('begin');

    await this.$axios.get('/plan-spend', {params:data})
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  },
  async week({ commit, store }, data){
    commit('begin');

    await this.$axios.post('/plan-spend/week/' + data.id, data);
  },
  async month({ commit, store }, data){
    commit('begin');

    await this.$axios.post('/plan-spend/month/' + data.id, data);
  },
}

export {state, mutations, getters, actions};
