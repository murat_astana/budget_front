export const state = () => ({
  user: null,
  token: null,
  error: null,
  isLoading: false
})

export const mutations = {
  begin(state){
    state.error = null;
    state.isLoading = true;
  },
  success(state, payload){
    if (payload.data)
      state.user = payload.data;

    state.error = null;
    state.isLoading = false;

    if (payload.token)
      state.token = payload.token;
  },
  error(state, error){
    state.error = error;
    state.isLoading = false;
    console.log(error);
  },
  clear(state){
    state.user = null;
    state.token = null;
    state.error = null;
    state.isLoading = false;
  }
}

export const actions = {
  async auth({ commit }, data){
    commit('begin');

    let request_data = {
      login: data.login,
      password: data.password
    };

    await this.$axios.post('/login', request_data)
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  },
  async logout({commit}, data){
    commit('clear');
  },
  async edit({commit}, data){
    commit('begin');

    await this.$axios.post('/profile', data)
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.response));
  }
}

export const getters = {
  user: state => state.user,
  error: state => state.error,
  token: state => state.token,
  isLoading: state => state.isLoading
}

