import {def_state, def_mutations, def_getters} from '@/proto/store/List'
import {mutationUpdate} from '@/proto/store/MutationUpdate'


const state =  () => ({...def_state});
const mutations = {...def_mutations}
const getters = {...def_getters}

mutations.update = mutationUpdate;

const actions = {
  async get({ commit }, data){
    commit('begin');

    await this.$axios.get('/user')
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  },
  async update({ commit, store }, data){
    commit('begin');

    await this.$axios.post('/user', data)
      .then(response => commit('update', response.data))
      .catch(error => commit('error', error.data));
  },
}

export {state, mutations, getters, actions};
