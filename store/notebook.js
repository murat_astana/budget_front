import {def_state, def_mutations, def_getters} from '@/proto/store/ListWithPage'
import {mutationUpdate} from '@/proto/store/MutationUpdate'
import {mutationCreate} from '@/proto/store/MutationCreate'
import {mutationDelete} from '@/proto/store/MutationDelete'


const state =  () => ({...def_state});
const mutations = {...def_mutations}
const getters = {...def_getters}

mutations.update = mutationUpdate;
mutations.create = mutationCreate;
mutations.delete = mutationDelete;

const actions = {
  async day({ commit }, data){
    commit('begin');

    await this.$axios.get('/notebook/list/day', {params:data})
      .then(response => commit('success', {data: response.data}))
      .catch(error => commit('error', error.data));
  },
  async list({ commit }, data){
    commit('begin');

    await this.$axios.get('/notebook', {params:data})
      .then(response => commit('success', response.data))
      .catch(error => commit('error', error.data));
  },
  async create({ commit }, data){
    commit('begin');

    await this.$axios.post('/notebook', data)
      .then(response => commit('create', response.data))
      .catch(error => commit('error', error.data));
  },
  async update({ commit, store }, data){
    commit('begin');

    await this.$axios.post('/notebook/' + data.id, data)
      .then(response => commit('update', response.data))
      .catch(error => commit('error', error.data));
  },
  async delete({ commit }, id){
    commit('begin');

    await this.$axios.delete('/notebook/' + id)
      .then(response => commit('delete', id))
      .catch(error => commit('error', error.data));
  },
  async load({state, dispatch}){
    console.log(state);
    if (state.data != null)
      return true;

    await dispatch('notebook');
  }
}

export {state, mutations, getters, actions};
