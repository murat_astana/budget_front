export default ({ app }, inject) => {
  inject('number_format', function(value, toHour = false){
    if (value == null)
      return 0;

    value = parseInt(value);
    if (value == NaN)
      return 0;
    if (toHour){
      return parseFloat(value/ 60).toFixed(2) + ' ч.';
    }

    return  value.toLocaleString('ru-RU');
  });
};
