export default function ({ $axios, $notify, redirect}) {
  /*
    $axios.onRequest(config => {
      console.log('Making request to ' + config.url)
    });
   */
  $axios.onError(error => {
    if (error.response.status == 401) {
      redirect('/login')
    }
    console.error(error.response)
  });
}
