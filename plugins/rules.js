

let rules = {
  required:  v => !!v || 'Это поле обязательно для заполнения',
  max:  v => v != undefined &&  v.length <= 255 || 'Это поле должно быть меньше 255 символов',
  min:  v => v != undefined && v.length >= 3 || ' Это поле должно быть больше 3 символов',
}

rules = new Proxy(rules, {
  get(target, phrase) {
    if (phrase in target) {
      return target[phrase]
    } else {
      return target['required']
    }
  }
})



export default ({ app }, inject) => {
  inject('rules', rules)
}
