
import moment from "moment";
import 'moment/locale/ru';
moment().format('ru');

export default ({ app }, inject) => {
  inject('dateparser', function(date, format = false){
    if (date == null)
      return '';

    if (format)
      return moment(date).format(format);

    return moment(date).format('DD.MM.YYYY');
  });
}
