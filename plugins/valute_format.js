export default ({ app }, inject) => {
  inject('valute_format', function(value, toHour = false){
    if (value == null)
      return 0;

    value = parseInt(value);
    if (value == NaN)
      return 0;

    return  value.toLocaleString('ru-RU');
  });
};
